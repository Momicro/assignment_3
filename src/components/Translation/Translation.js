import { useState } from 'react'
import { useSelector } from 'react-redux' 
import { Link, Redirect } from 'react-router-dom' 
import AppContainer from '../../hoc/AppContainer' 
import Signs from '../Signs/Signs'

const Translation = () => {

    const {loggedIn} = useSelector(state => state.sessionReducer) 

    const [ user, setUser ] = useState({
        username: '', 
        sign: ''
    })

    const onSubmit = event => {
        event.preventDefault() 
        console.log('Translation.onSubmit()', user) 
    }

    const onInputChange = event => {
        setUser({
            ...user, 
            [event.target.id]: event.target.value 
        })
    }

    const handleSignChanged = sign => {
        setUser({
            ...user,
            sign: sign.url
        })
    }

    return (
        <AppContainer>
            {loggedIn && <Redirect to="/translation" />} 
            <form className="mb-3" onSubmit={onSubmit}>
                <h1>Register for the Lost Translation</h1> 
                <p>Complete the form to create your account</p> 

                <div className="mb-3">

                    <input onChange={onInputChange} type="text" id="name" 
                    className="form-control" placeholder="What's your name?" />
                </div> 

                <button className="btn btn-success btn-lg">OK</button>

            </form> 

            <Signs aaa={user.username} changed={handleSignChanged} /> 

        </AppContainer>
    )
}
export default Translation
