import { useState } from 'react'
import { useSelector } from 'react-redux' 
import { Link, Redirect } from 'react-router-dom' 
import AppContainer from '../../hoc/AppContainer' 
import Signs from '../Signs/Signs'

const Register = () => {

    const {loggedIn} = useSelector(state => state.sessionReducer) 

    const [ user, setUser ] = useState({
        username: '', 
        password: '', 
        confirmPassword: '', 
        sign: ''
    })

    const onRegisterSubmit = event => {
        event.preventDefault() 
        console.log('Register.onRegisterSubmit()', user) 
    }

    const onInputChange = event => {
        setUser({
            ...user, 
            [event.target.id]: event.target.value 
        })
    }

    const handleSignChanged = sign => {
        setUser({
            ...user,
            sign: sign.url
        })
    }

    return (
        <AppContainer>
            {loggedIn && <Redirect to="/translation" />} 
            <form className="mb-3" onSubmit={onRegisterSubmit}>
                <h1>Register for the Lost Translation</h1> 
                <p>Complete the form to create your account</p> 

                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Choose a username *</label> 
                    <input onChange={onInputChange} type="text" id="username" className="form-control" placeholder="johndoe" />
                </div> 

                <div className="mb-3">
                    <label htmlFor="password" className="form-label">Choose a password *</label> 
                    <input onChange={onInputChange} type="password" id="password" className="form-control" placeholder="*****" />
                </div>

                <div className="mb-3">
                    <label htmlFor="confirmPassword" className="form-label">Confirm your password *</label> 
                    <input onChange={onInputChange} type="password" id="confirmPassword" className="form-control" placeholder="*****" />
                </div> 

                <Signs changed={handleSignChanged} /> 

                <button className="btn btn-success btn-lg">Register</button>

            </form> 
            <p className="mb-3">
                <Link to="/">Already register? Login here</Link>
            </p>
        </AppContainer>
    )
}
export default Register
