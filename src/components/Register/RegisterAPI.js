import { handleFirstResponse } from "../../utils/apiUtils"

export const RegisterAPI = {
    register({ username, password, sign }) {
        fetch('https://noroff-react-txt-forum-api.herokuapp.com/users/register', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json' 
            }, 
            body: JSON.stringify({ username, password, sign }) 
        }).then(handleFirstResponse) 
    }
}