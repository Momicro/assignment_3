import { handleFirstResponse } from "../../utils/apiUtils"

export const signsAPI = {
    getSigns() {
        return fetch('http://localhost:8000/signs')
        .then(handleFirstResponse) 
    }
}
