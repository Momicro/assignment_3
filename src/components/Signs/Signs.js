import { useState } from "react"
import { useEffect } from "react"
import { signsAPI } from "./SignsAPI"
import styles from './Signs.module.css' 
import SignsImage from "./SignsImage"

const Signs = ({changed}) => { 

    const [state, setState] = useState({
        Signs: [], 
        fetching: true, 
        selectedSign: -1, 
        error: ''
    })

    useEffect(() => {
        signsAPI.getSigns()
            .then(signs => {
                setState({
                    ...state,
                    error: '',
                    fetching: false,
                    signs
                })
            })
            .catch(error => {
                setState({
                    ...state,
                    error: error.message,
                    fetching: false, 
                })
            })
    }, [])
    
    const handleSignChanged = sign => {
        setState({
            ...state,
            selectedSign: sign
        })
        changed(sign) 
    } 

    return (
        <section>
            {state.fetching && <p>Signs are loading...</p>} 
            <div className={styles.Signs}>
                {state.signs.map(sign => {
                    return <SignsImage sign={sign}
                    selected={sign.id === state.selectedSign.id}
                    key={sign.id} 
                    clicked={handleSignChanged} /> }) }
            </div>
        </section>
    )
}
export default Signs
