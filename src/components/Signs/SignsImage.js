import styles from './Signs.module.css' 


const SignsImage = ({sign, selected, clicked}) => { 

    const classNames = [styles.SignsImage] 

    if(selected === true) {
        classNames.push(styles.SignsImageSelected)
    }

    const onSignClick = () => {
        clicked(sign)
    }
    return (
        <figure>
            <img className={classNames.join(' ')} src={ sign.url } alt="Sign" onClick={onSignClick}/>
        </figure>
    )
}
export default SignsImage
