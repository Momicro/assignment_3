export const handleFirstResponse = async Response => {
    if (!Response.ok) {
        const {error = 'An unknown error occurred.'} = await Response.json()
        throw new Error(error)
    }
    return Response.json()
}