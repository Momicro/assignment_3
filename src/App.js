import './App.css';
import {
  BrowserRouter, 
  Switch, 
  Route, 
  Redirect 
} from 'react-router-dom'; 
import AppContainer from './hoc/AppContainer';
import Login from './components/Login/Login'; 
import Register from './components/Register/Register'; 
import NotFound from './components/NotFound/NotFound';
import Translation from './components/Translation/Translation';
// Icons in: https://fonts.google.com/icons?selected=Material+Icons

function App() {
  return (
    <BrowserRouter>
      <div className="App"> 
      <AppContainer>
        <h1>React Lost Translation</h1>
        
      </AppContainer>

        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="/register" component={ Register } />
          <Route path="/translation" component={ Translation } />
          <Route path="*" component={ NotFound } />
        </Switch>

        { /* <footer>Hier is the Footer</footer> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
